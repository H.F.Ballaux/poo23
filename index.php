<?php

spl_autoload_register(function ($class) {
    require __DIR__ . '/' . strtolower(str_replace('\\', DIRECTORY_SEPARATOR, $class)) . '.php';
});

$weapon = \classes\weapon::generateWeapon();
$magic = \classes\magic::generateMagic();

$pc = new \classes\pc('PC', 1, 'fabio');
$pc->setPv(100);
$pc->setMana(12);
$pc->setAtk(68);
$pc->setDef(57);
$pc->giveWeapon($weapon);
$pc->giveMagic($magic);

$npc = new \classes\npc('NPC', 2);
$npc->setPv(100);
$npc->setMana(12);
$npc->setAtk(57);
$npc->setDef(68);
$npc->giveWeapon($weapon);
$npc->giveMagic($magic);

$tab_user = [$pc, $npc];

require 'structure/header.html';
echo '
    ------------------------------------------------------ <br>
    |&nbsp;&nbsp;&nbsp;Fight<br>
    ------------------------------------------------------ <br>
    | ' . $pc->showCharacter() . '
    | &nbsp; &nbsp; Vs <br>
    | ' . $npc->showCharacter() . '
    ------------------------------------------------------ <br>
';

require 'structure/side.html';
foreach ($tab_user as $user) {
    echo '<div class="profil">' . $user->showCharacterComplete() . '</div>';
}

require 'structure/bottom.html';
echo '------------------------------------------------------ <br>';
$cpt = 1;
while ($pc->getPv() > 0 && $npc->getPv() > 0) {
    echo '| Tour ' . $cpt . '<br>';
    echo $pc->attack($npc);
    if ($npc->getPv() > 0 && $pc->getPv() > 0) {
        echo $npc->attack($pc);
    }
    $cpt += 1;
    echo '-------<br>';
}

$winner = ($pc->getPv() >= 0) ? $pc : $npc;
echo ' And the winner is... '.$winner->getName().'<br>';
echo '------------------------------------------------------ <br>';

require 'structure/footer.html';
