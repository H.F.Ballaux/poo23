function changeColor(color){
    let backgroundColorBody;
    let backgroundColorMain;
    let fontColor;
    switch (color){
        case 'red':
            backgroundColorBody = '#933';
            backgroundColorMain = '#b55';
            fontColor = '#eaa';
            break;
        case 'green':
            backgroundColorBody = '#393';
            backgroundColorMain = '#5b5';
            fontColor = '#aea';
            break;

        case 'yellow':
            backgroundColorBody = '#993';
            backgroundColorMain = '#bb5';
            fontColor = '#eea';
            break;

        case 'blue':
            backgroundColorBody = '#339';
            backgroundColorMain = '#55b';
            fontColor = '#aae';
            break;
        default:
            backgroundColorBody = '#333';
            backgroundColorMain = '#555';
            fontColor = '#999';
    }
    document.getElementById('body').style.backgroundColor = backgroundColorBody;
    document.getElementById('body').style.color = fontColor;
    document.getElementById('main').style.backgroundColor = backgroundColorMain;
}
