<?php

namespace classes;

class corps_a_corps extends weapon
{
    protected int $taille;

    /**
     * @param int $id
     */
    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->setParade(true);
    }

    public function getPortee(): string
    {
        return $this->taille.'cm';
    }

    public function getTaille(): int
    {
        return $this->taille;
    }

    public function setTaille(int $taille): void
    {
        $this->taille = $taille;
    }



}