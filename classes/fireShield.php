<?php

namespace classes;

class fireShield extends magic
{
    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->setName('FireShield');
        $this->setType('Fire');
        $this->setCategory(capacity::CAT_DEF);
        $this->setDefense(15);
        $this->setMinDamage(2);
        $this->setMaxDamage(15);
    }
}