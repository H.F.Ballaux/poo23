<?php

namespace classes;

final class axe extends corps_a_corps
{
    /**
     * @param int $id
     */
    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->setName('Hache');
        $this->setCategory(capacity::CAT_OFF);
        $this->setDefense(1);
        $this->setTaille(85);
        $this->setMinDamage(6);
        $this->setMaxDamage(13);
    }
}