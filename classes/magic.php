<?php

namespace classes;

abstract class magic extends capacity
{
    private string $type;
    protected int $category;

    /**
     * @param int $id
     */
    public function __construct(int $id)
    {
        parent::__construct($id);
    }

    public function getCategory(): int
    {
        return $this->category;
    }

    public function setCategory(int $category): void
    {
        $this->category = $category;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public static function generateMagic(): array
    {
        return [
            new blizzard(1),
            new lightning(2),
            new fireblast(3),
            new earthquake(4),
            new iceShield(5),
            new electricShield(6),
        ];
    }
}