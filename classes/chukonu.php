<?php

namespace classes;

final class chukonu extends distance
{
    /**
     * @param int $id
     */
    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->setDistance(60);
        $this->setName("Chu-ko-nu");
        $this->setCategory(capacity::CAT_OFF);
        $this->setDefense(1);
        $this->setProjectile(3);
        $this->setMinDamage(1);
        $this->setMaxDamage(16);
    }

}