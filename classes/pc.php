<?php

namespace classes;

final class pc extends character
{
    protected string $pseudo;

    /**
     * @param string $name
     * @param int $id
     * @param string $pseudo
     *
     */
    public function __construct(string $name, int $id, string $pseudo)
    {
        parent::__construct($name, $id);
        $this->pseudo = $pseudo;
        $this->setName($this->pseudo);
    }

    public function getPseudo(): string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): void
    {
        $this->pseudo = $pseudo;
    }

}