<?php

namespace classes;

final class fireblast extends magic
{
    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->setName('Fireblast');
        $this->setType('feux');
        $this->setCategory(capacity::CAT_OFF);
        $this->setDefense(1);
        $this->setMinDamage(2);
        $this->setMaxDamage(15);
    }
}