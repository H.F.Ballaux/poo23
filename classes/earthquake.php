<?php

namespace classes;

final class earthquake extends magic
{
    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->setName('Earthquake');
        $this->setType('Terre');
        $this->setCategory(capacity::CAT_OFF);
        $this->setDefense(1);
        $this->setMinDamage(2);
        $this->setMaxDamage(15);
    }

}