<?php

namespace classes;

class iceShield extends magic
{
    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->setName('IceShield');
        $this->setType('Ice');
        $this->setCategory(capacity::CAT_DEF);
        $this->setDefense(15);
        $this->setMinDamage(2);
        $this->setMaxDamage(15);
    }
}