<?php

namespace classes;

final class npc extends character
{
    public int $killed;

    /**
     * @param string $name
     * @param int $id
     */
    public function __construct(string $name, int $id)
    {
        parent::__construct($name, $id);
    }

    public function getKilled(): int
    {
        return $this->killed;
    }

    public function setKilled(int $killed): void
    {
        $this->killed = $killed;
    }


}