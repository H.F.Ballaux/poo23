<?php

namespace classes;

final class longbow extends distance
{

    /**
     * @param int $id
     */
    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->setName("Arc Long");
        $this->setCategory(capacity::CAT_OFF);
        $this->setDefense(1);
        $this->setDistance(100);
        $this->setProjectile(1);
        $this->setMinDamage(1);
        $this->setMaxDamage(16);
    }
}