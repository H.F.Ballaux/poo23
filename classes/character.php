<?php

namespace classes;

abstract class character
{
    public string $id;
    protected string $name;
    protected int $mana;
    protected string $pv;
    protected string $atk;
    protected string $def;
    protected weapon $weapon;
    protected magic $magic;

    /**
     * @param string $name
     * @param string $id
     */
    public function __construct(string $name, string $id)
    {
        $this->name = $name;
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getMana(): int
    {
        return $this->mana;
    }

    public function setMana(int $mana): void
    {
        $this->mana = $mana;
    }

    public function getPv(): string
    {
        return $this->pv;
    }

    public function setPv(string $pv): void
    {
        $this->pv = $pv;
    }

    public function getAtk(): string
    {
        return $this->atk;
    }

    public function setAtk(string $atk): void
    {
        $this->atk = $atk;
    }

    public function getDef(): string
    {
        return $this->def;
    }

    public function setDef(string $def): void
    {
        $this->def = $def;
    }

    public function getWeapon(): weapon
    {
        return $this->weapon;
    }

    public function setWeapon(weapon $weapon): void
    {
        $this->weapon = $weapon;
    }

    public function getMagic(): magic
    {
        return $this->magic;
    }

    public function setMagic(magic $magic): void
    {
        $this->magic = $magic;
    }
    public function showCharacterComplete():string{
        return '
        nom : '.$this->name.'<br>
        PV : '.$this->pv.'<br>
        atk : '.$this->atk.'<br>
        def : '.$this->def.'<br>
        arme : <br>
        &nbsp;&nbsp;&nbsp;- nom : '.$this->getWeapon()->getName().'<br>
        &nbsp;&nbsp;&nbsp;- portée : '.$this->getWeapon()->getPortee().'<br>
        &nbsp;&nbsp;&nbsp;- categorie : '.$this->getWeapon()->getCategory().'<br>
        magie :<br>
        &nbsp;&nbsp;&nbsp;- nom : '.$this->getMagic()->getName().'<br>
        &nbsp;&nbsp;&nbsp;- type : '.$this->getMagic()->getType().'<br>
        &nbsp;&nbsp;&nbsp;- categorie : '.$this->getMagic()->getCategory().'<br>
        ';
    }
    public function showCharacter(): string
    {
        return $this->getName().' - '.$this->getWeapon()->getName().' - '.$this->getMagic()->getName().' - '.$this->getPv().' PV<br>';
    }

    public function pary(): bool
    {
        $pary_percent_chances = floor($this->getAtk()/10)  ;
        $rand = rand(0,10);
        return $rand <= $pary_percent_chances;
    }

    public function dodge(): bool
    {
        $dodge_percent_chances = floor($this->getDef()/10);
        $rand = rand(1,10);
        return $rand<= $dodge_percent_chances;
    }

    public function giveMagic(array $magic):void{
        $this->magic = $magic[rand(0, sizeof($magic)-1)];
    }

    public function giveWeapon(array $weapon): void
    {
        $this->weapon = $weapon[rand(0, sizeof($weapon)-1)];
    }

    public function attack(character $target): string
    {
        $atk = rand(1, $this->getAtk());
        $pary = false;
        $dodge = false;
        $def = 0;
        if ($this->getMagic()->getCategory() == capacity::CAT_OFF && $this->getMana() >= 4){
            $attaque = $this->getMagic();
            $this->setMana($this->getMana()-4);
        } elseif ($this->getWeapon()->getCategory()== capacity::CAT_OFF){
            $attaque = $this->getWeapon();
            $def = $target->getDef();
            if (is_a($this->getWeapon(), 'classes\corps_a_corps') && $target->getWeapon()->getCategory() == capacity::CAT_DEF ){
                $pary = $target->pary();
            }
            if (is_a($target->getWeapon(), 'classes\distance')){
                $dodge = $target->dodge();
            }
            if ($target->getMagic()->getCategory() == capacity::CAT_DEF){
                $def += $target->getMagic()->getDefense();
            }
        } else {
            $this->setMana($this->getMana()+3);
            return '| '. $this->getName().' Ne possede pas d\'atout offensif, son tour est avorté<br>';
        }

        $result = $atk - rand(1,$def);

        if ($result>0){
            if ($pary){
                $string = $target->getName(). ' bloque grace a '. $target->getWeapon()->getName(). ' et contre attaque <br>';
                $string.= $target->attack($this);
            } elseif ($dodge){
                $string = $target->getName(). ' esquive, aucun dégats <br>';
            } else {
                $vie = $target->getPv() - $attaque->getDamage();
                $string = ' Touché '. $target->getName(). ' est ';
                if ($vie>0){
                    $string.= 'blessé, il lui reste '. $vie .' PV' ;
                } else {
                    $string .= 'Mort! ';
                }
                $target->setPv($vie);
            }
        } elseif ($result < 0){
            $string = ' C\'est raté!';
        } else{
            $string = ' C\'est paré! ';
        }
        $this->setMana($this->getMana()+1);
        return '| '.$this->name .' attaque '. $target->name .' avec ' . $attaque->getName() .' <br>| - '.$string .' <br>';
    }
}