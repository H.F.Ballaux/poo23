<?php

namespace classes;

class distance extends weapon
{
    protected string $distance;
    protected int $projectile;

    /**
     * @param int $id
     */
    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->setParade(false);
    }

    public function getPortee(): string
    {
        return $this->distance.'m';
    }

    public function getDistance(): string
    {
        return $this->distance;
    }

    public function setDistance(string $distance): void
    {
        $this->distance = $distance;
    }

    public function getParrade(): string
    {
        return $this->parrade;
    }

    public function setParrade(string $parrade): void
    {
        $this->parrade = $parrade;
    }

    public function getProjectile(): int
    {
        return $this->projectile;
    }

    public function setProjectile(int $projectile): void
    {
        $this->projectile = $projectile;
    }
}