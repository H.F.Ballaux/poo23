<?php

namespace classes;

final class sword extends corps_a_corps
{
    /**
     * @param int $id
     */
    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->setName("Epee");
        $this->setTaille(85);
        $this->setCategory(capacity::CAT_OFF);
        $this->setDefense(1);
        $this->setPortee($this->getTaille());
        $this->setMinDamage(5);
        $this->setMaxDamage(15);
    }
}