<?php

namespace classes;

class electricShield extends magic
{
    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->setName('ElectricShield');
        $this->setType('Electric');
        $this->setCategory(capacity::CAT_DEF);
        $this->setDefense(15);
        $this->setMinDamage(2);
        $this->setMaxDamage(15);
    }
}