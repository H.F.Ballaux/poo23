<?php

namespace classes;

use classes\corps_a_corps;

class shield extends corps_a_corps
{
    /**
     * @param int $id
     */
    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->setName('Bouclier');
        $this->setCategory(capacity::CAT_DEF);
        $this->setDefense(15);
        $this->setTaille(75);
        $this->setMinDamage(6);
        $this->setMaxDamage(13);
    }
}