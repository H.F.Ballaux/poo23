<?php

namespace classes;

abstract class weapon extends capacity
{
    private bool $parade;
    protected int $portee;
    protected int $category;

    /**
     * @param int $id
     */
    public function __construct(int $id)
    {
        parent::__construct($id);
    }

    public function getCategory(): int
    {
        return $this->category;
    }

    public function setCategory(int $category): void
    {
        $this->category = $category;
    }

    public function getPortee(): string
    {
        return $this->portee;
    }

    public function setPortee(int $portee): void
    {
        $this->portee = $portee;
    }

    public function isParade(): bool
    {
        return $this->parade;
    }

    public function setParade(bool $parade): void
    {
        $this->parade = $parade;
    }

    public static function generateWeapon(): array
    {
        return [
            new sword(1),
            new axe(2),
            new longbow(3),
            new chukonu(4),
            new shield(5)
        ];
    }
}