<?php

namespace classes;

abstract class capacity
{
    public int $id;
    protected string $name;
    protected int $minDamage;
    protected int $maxDamage;
    protected int $defense;
    protected int $category;

    private static array $categories = [self::CAT_DEF, self::CAT_OFF];

    const CAT_OFF = 1;
    const CAT_DEF = 2;

    /**
     * @param int $id
     * @param int $category
     */
    public function __construct(int $id, int $category = self::CAT_OFF)
    {
        $this->id = $id;
        if (!in_array($category, self::$categories)){
            $category = self::CAT_OFF;
        }
        $this->category = $category;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getMinDamage(): int
    {
        return $this->minDamage;
    }

    public function setMinDamage(int $minDamage): void
    {
        $this->minDamage = $minDamage;
    }

    public function getMaxDamage(): int
    {
        return $this->maxDamage;
    }

    public function setMaxDamage(int $maxDamage): void
    {
        $this->maxDamage = $maxDamage;
    }

    public function getDefense(): int
    {
        return $this->defense;
    }

    public function setDefense(int $defense): void
    {
        $this->defense = $defense;
    }

    public function getDamage(): int
    {
        return rand($this->getMinDamage(), $this->getMaxDamage());
    }
}