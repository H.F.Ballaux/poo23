<?php

namespace classes;

final class blizzard extends magic
{
    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->setName('Blizzard');
        $this->setType('Glace');
        $this->setCategory(capacity::CAT_OFF);
        $this->setDefense(1);
        $this->setMinDamage(2);
        $this->setMaxDamage(15);
    }
}