<?php

namespace classes;

final class lightning extends magic
{
    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->setName('Foudre');
        $this->setType('Electricitée');
        $this->setCategory(capacity::CAT_OFF);
        $this->setDefense(1);
        $this->setMinDamage(2);
        $this->setMaxDamage(15);
    }
}