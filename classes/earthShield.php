<?php

namespace classes;

class earthShield extends magic
{
    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->setName('EarthShield');
        $this->setType('Earth');
        $this->setCategory(capacity::CAT_DEF);
        $this->setDefense(15);
        $this->setMinDamage(2);
        $this->setMaxDamage(15);
    }
}